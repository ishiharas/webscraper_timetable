const mongoose = require('mongoose');
const scrape = require('./scrapeLinks.js');

const url = 'mongodb://localhost:27017/timetablescraper';

mongoose.connect(url, { useNewUrlParser: true }).then((db) => {
  console.log('i feel so connected to Everyone');
  db = mongoose.connection;

  const data = db.collection('links');

  scrape().then((metadata) => {
    const toProcess = metadata.length;
    let done = 0;
    let inserted = 0;
    console.log(`got my result, size is ${toProcess}`);

    metadata.forEach((meta) => {
      meta.timestamp = new Date();

      data.find({
        abbreviation: meta.abbreviation,
        semester: meta.semester,
        sets: meta.sets,
        group: meta.group,
      }).toArray((err, docs) => {
        if (err) console.log('Err', err);
        if (docs && docs.length === 0) {
          data.insertOne(meta, (err) => {
            if (err) throw (err);
            if (!err) {
              inserted += 1;
              done += 1;
              if (done === toProcess) {
                db.close();
                console.log('Total inserted: ', inserted);
              }
            }
          });
        } else {
          // console.log('not inserting');
          done += 1;
          if (done === toProcess) {
            db.close();
            console.log('Total inserted: ', inserted);
          }
        }
      });
    });
  }).catch((err) => {
    console.log('unhandled error', err);
    db.close();
  });
}).catch((err) => {
  console.log('mongodb err', err);
});

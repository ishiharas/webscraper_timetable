const request = require('request');
const cheerio = require('cheerio');

function getData() {
  return new Promise(((resolve, reject) => {
    request('http://stundenplanung.eah-jena.de/inc/StudentSetList.js', (err, response, html) => {
      if (err) reject(err);
      if (response.statusCode !== 200) {
        reject(`Invalid status code: ${response.statusCode}`);
      }

      const $ = cheerio.load(html);
      const list = $('body');
      const jsSetList = list.text().split('\'');

      // get and create array of each study course available
      const sortedSetListArray = [];

      jsSetList.forEach((item) => {
        if (item.match('#')) {
          sortedSetListArray.push(item);
        }
      });

      // split generated list in components
      const metadata = [];

      sortedSetListArray.forEach((item) => {
        const abbreviation = item.substring(0, item.indexOf(')') + 1);
        const semester = item.substring(item.indexOf(')') + 1, item.indexOf('.'));
        const sets = item.substring(item.indexOf('.') + 1, item.indexOf('#'));
        const group = item.split('#');

        metadata.push({
          abbreviation,
          semester,
          sets,
          group: group[1],
        });
      });
      resolve(metadata);
    });
  }));
}
module.exports = getData;

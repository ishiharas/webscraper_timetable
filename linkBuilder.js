const mongoose = require('mongoose');

function getData() {
  return new Promise(((resolve, reject) => {
    const url = 'mongodb://localhost:27017/timetablescraper';
    mongoose.connect(url, { useNewUrlParser: true });
    const db = mongoose.connection;

    db.on('error', console.error.bind(console, 'MongoDB connection error:'));

    const Schema = mongoose.Schema;
    const LinksSchema = new Schema({
      abbreviation: String,
      semester: Number,
      sets: Number,
      group: String,
      timestamp: Date,
    });

    const LinkModel = mongoose.model('links', LinksSchema);
    const query = LinkModel.find({});
    query.select('abbreviation semester group');
    const linkArray = [];

    function getLinksPromise() {
      const promise = LinkModel.find({}).exec();
      return promise;
    }

    const promise = getLinksPromise();

    promise.then((links) => {
      links.forEach((link) => {
        const baserUrl = 'http://stundenplanung.eah-jena.de/studentset/?Studiengang=';
        const divider = '&';
        const preSemester = 'Semester=';
        const preGroup = 'Group=';
        const type = 'type=list&week=1-15&template=set';

        const semester = link.semester;
        const abbreviation = link.abbreviation;
        const group = link.group;

        linkArray.push(
          baserUrl
                    + abbreviation
                    + divider
                    + preSemester
                    + semester
                    + divider
                    + preGroup
                    + group
                    + divider
                    + type,
        );
      });
      resolve(linkArray);
      db.close();
    }).catch((error) => {
      reject(error);
      console.log(error);
    });
  }));
}

module.exports = getData;

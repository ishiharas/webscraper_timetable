const request = require('request');
const cheerio = require('cheerio');
const links = require('./linkBuilder');


async function getLinks() {
  try {
    const linkArray = await links();
    return linkArray;
  } catch (rejectedValue) {
    console.log(`error ${rejectedValue}`);
    return rejectedValue;
  }
}

async function getData(linkFunction) {
  const linkArray = await linkFunction;
  linkArray.map((link) => {
    request(link, (err, response, html) => {
      const $ = cheerio.load(html);
      const innerText = $('.ts_planTable tr td');
      console.log(innerText[10]);
    });
    // console.log(link);
  });
  // console.log(linkArray[0]);
}

getData(getLinks());

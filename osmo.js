const osmosis = require('osmosis');

const url = ('http://stundenplanung.eah-jena.de/studentset/?Studiengang=AO%28BA%29&Semester=2&Group=SPLUS19B824&type=list&week=1-2&template=set');

function getFullWeekList() {
  return new Promise((resolve, reject) => {
    const list = [];

    osmosis
      .get(url)
      .find('.ts_planTable:first(3) tr')
      .set({
        startTime: 'td[1]',
        endTime: 'td[2]',
        event: 'td[3]',
        lecturer: 'td[4]',
        room: 'td[5]',
        weeks: 'td[6]',
        beginn: 'td[7]',
      })
      .data(data => list.push(data))
      .error(err => reject(err))
      .done(() => resolve(list));
  });
}

getFullWeekList().then((list) => {
  // First I need to slice the list from first position [0] to
  // the position of the second occurence of
  // { startTime: 'anfang'} and create a new list called 'monday'.
  // Than take the old index of { startTime: 'Anfang'} together
  // with the next occurence of { startTime: 'Anfang'} and
  // create a new list called 'tuesday'. When finished, the new
  // list arrays need to get a .push with the new weekday value.
  const weekDay = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  const searchVal = 'Anfang';

  const indices = [];

  let i;
  for (i = 0; i < list.length; i++) {
    if (list[i].startTime === searchVal) indices.push(i);
  }

  let y;
  let x;
  for (x = 0; x < weekDay.length; x++) {
    for (indices[0]; y < indices[1]; y++) {
      Object.assign(list[y], { weekDay: weekDay[x] });
    }
    indices.shift();
  }

  console.log(list);
  console.log(indices);
}).catch(err => console.log(err));
